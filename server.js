var path = require("path");
var express = require("express");
var logger = require("morgan");
var bodyParser = require("body-parser");

var app = express();  // make express app
var server = require('http').createServer(app);




// set up the view engine
app.set("views", path.resolve(__dirname, "views")); // path to views
app.set("view engine", "ejs"); // specify our view engine
// manage our entries
var entries = [];
app.locals.entries = entries;
// set up the logger
app.use(logger("dev"));
app.use(bodyParser.urlencoded({ extended: false }));

app.use(express.static(path.join(__dirname+'/assets/')));

app.get("/", function(request,response){
  response.sendFile(path.join(__dirname+'/assets/Home.html'));
});

// GETS
app.get("/", function (request, response) {
  response.render("index");
});
app.get("/guestbook", function (request, response) {
  response.render("index");
});

app.get("/new-entry", function (request, response) {
  response.render("new-entry");
});

app.get("/contact.html", function(request,response){
  response.render("contact.html");
});



// POSTS

app.post("/contact.html", function(request,response){
var api_key = 'key-c3d6e6853c37a4bf81089796a7dd8621';
var domain = 'sandboxb6ea7a7878aa466a85fc1aee174b5e85.mailgun.org';
var mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});
 
var data = {
  from: 'Mail gun Santosh <postmaster@sandboxb6ea7a7878aa466a85fc1aee174b5e85.mailgun.org>',
  to: 'gsr.teja@gmail.com',
  subject: request.body.title + " sent you a MESSAGE",
  html: "<b style = 'font-family: Palatino Linotype, Book Antiqua, Palatino, serif'>MESSAGE:</b> "+ request.body.body
  };
 
mailgun.messages().send(data, function (error, body) {
  console.log(body);
  response.send("Your Mail to the Admin has been Sent successfully!");
});
});

app.post("/new-entry", function (request, response) {
  if (!request.body.title || !request.body.body) {
    response.status(400).send("Entries must have a title and a body.");
    return;
  }
  entries.push({
    title: request.body.title,
    content: request.body.body,
    published: new Date()
  });
  response.redirect("guestbook");
});

// 404
app.use(function (request, response) {
  response.status(404).render("404");
});


// Listen for an application request on port 8081
server.listen(8081, function () {
  console.log('Guestbook app and contact  listening on http://127.0.0.1:8081/');
});

