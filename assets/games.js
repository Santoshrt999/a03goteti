 //Set up an associative array
 //The keys represent the console for the game.
 //The values represent the cost of the console.
 var console_prices = new Array();
 console_prices["Round6"]=20;
 console_prices["Round8"]=25;
 console_prices["Round10"]=35;
 console_prices["Round12"]=75;
 
 //Set up an associative array 
 //The keys represent the game type
//We use this this array when the user selects a game from the form
 var gaming_prices= new Array();
 gaming_prices["None"]=0;
 gaming_prices["GTA"]=5;
 gaming_prices["games 1.1"]=10;
 gaming_prices["avengerZZ"]=15;
 gaming_prices["fultron"]=20;
 gaming_prices["Raspberry PI"]=25;
 gaming_prices["hunting"]=30;
 gaming_prices["shooting ghost protocol"]=35;
 gaming_prices["talking jim"]=40;               
 gaming_prices["bubble cup"]=45;
 gaming_prices["Apricot"]=50;
 gaming_prices["Bet man"]=55;
 gaming_prices["siper man"]=60;
 
	 
	 
// getConsolePrice() finds the price of the console.
// Here, we need to take user's the selection from radio button selection
function getConsolePrice()
{  
    var consolePrice=0;
    //Get a reference to the form id="gameform""
    var theForm = document.forms["gameform"];
    //Get a reference to the cake the user Chooses name=selectedconsole":
    var selectedconsole = theForm.elements["selectedconsole"];
    //Here since there are 4 radio buttons selectedconsole.length = 4
    //We loop through each radio buttons
    for(var i = 0; i < selectedconsole.length; i++)
    {
        //if the radio button is checked
        if(selectedconsole[i].checked)
        {
            //we set consolePrice to the value of the selected radio button
            //i.e. if the user choose the 8" cake we set it to 25
            //by using the console_prices array
            //We get the selected Items value
            //For example console_prices["Round8".value]"
            consolePrice = console_prices[selectedconsole[i].value];
            //If we get a match then we break out of this loop
            //No reason to continue if we get a match
            break;
        }
    }
    //We return the consolePrice
    return consolePrice;
}

//This function finds the game price based on the 
//drop down selection
function getGamePrice()
{
    var gamePrice=0;
    //Get a reference to the form id="gameform""
    var theForm = document.forms["gameform"];                 
    //Get a reference to the select id="game"
     var selectedgame = theForm.elements["game"];
     
    //set game Price equal to value user chose 
    //For example gaming_prices["Lemon".value] would be equal to 5
    gamePrice = gaming_prices[selectedgame.value];

    //finally we return gamePrice
    return gamePrice;
}

//crackPrice() finds the crack Price price based on a check box selection
function crackPrice()
{
    var crackPrice=0;
    //Get a reference to the form id="gameform""
    var theForm = document.forms["gameform"];
    //Get a reference to the checkbox id="includecrack"
    var includecrack = theForm.elements["includecrack"];

    //If they checked the box set crackPrice to 5
    if(includecrack.checked==true)
    {
        crackPrice=5;
    }
    //finally we return the crackPrice
    return crackPrice;
}

function versionPrice()
{
    //This local variable will be used to decide whether or not to charge for the version
    //If the user checked the box this value will be 20
    //otherwise it will remain at 0
    var versionPrice=0;
    //Get a refernce to the form id="gameform"
    var theForm = document.forms["gameform"];
    //Get a reference to the checkbox id="includeversion"
    var includeversion = theForm.elements["includeversion"];
    //If they checked the box set versionPrice to 20
    if(includeversion.checked==true){
        versionPrice=20;
    }
    //finally we return the versionPrice
    return versionPrice;
}
        
function calculateTotal()
{
    //Here we get the total price by calling our function
    //Each function returns a number so by calling them we add the values they return together
    var gamePrice = sum(getConsolePrice(),getGamePrice(),crackPrice(),versionPrice());
    
    //display the result
    var divobj = document.getElementById('totalPrice');
    
    divobj.style.display='block';
    divobj.innerHTML = "Total Price For the gaming pack is $"+ gamePrice;

}

function hideTotal()
{
    //var divobj = document.getElementById('totalPrice');
   var divobj =  $("#totalPrice");
    divobj.style.display='none';
}

function sum(first, second, third, fourth){
    if(!isNaN(first) && !isNaN(second) && !isNaN(third)  && !isNaN(fourth))
        return first+ second + third + fourth;
    else
        throw Error("only numbers are allowed");
}
